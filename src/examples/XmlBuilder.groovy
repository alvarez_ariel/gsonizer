package examples

/**
 * TADP - UTN FRBA 
 *
 * User: aalvarez
 * Date: 18/05/13
 * Time: 07:35
 */
class XmlBuilder {

    def out

    XmlBuilder(out) { this.out = out }

    def invokeMethod(String name, args) {
        out << "<$name>"
        if(args[0] instanceof Closure) {
            args[0].delegate = this
            args[0].call()
        }
        else {
            out << args[0].toString()
        }
        out << "</$name>"
    }
}
