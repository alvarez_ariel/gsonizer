package examples

/**
 * TADP - UTN FRBA 
 *
 * User: aalvarez
 * Date: 18/05/13
 * Time: 08:05
 */
class Delegador {
    def responsable

    def getProperty(String name) {
        if (responsable)
            responsable[name]
        else{
            def metaMethod = this.metaClass.getMetaMethod(name, [])
            return metaMethod.invoke(this)
        }
    }

    void setProperty(String name, value) {
        if(responsable)
            responsable[name]
        else{
            def metaMethod = this.metaClass.getMetaMethod(name, [value])
            metaMethod.invoke(this, [value])
        }
    }

}


//class Expandable {
//    def storage = [:]
//    def getProperty(String name) { storage[name] }
//    void setProperty(String name, value) { storage[name] = value }
//}
//
//def e = new Expandable()
//e.foo = "bar"
//println e.foo