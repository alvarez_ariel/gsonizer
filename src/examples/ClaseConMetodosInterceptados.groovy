package examples

/**
 * TADP - UTN FRBA 
 *
 * User: aalvarez
 * Date: 18/05/13
 * Time: 07:48
 */
class ClaseConMetodosInterceptados implements GroovyInterceptable {

    def invokeMethod(String name, args) {
        System.out.println ("Estoy interceptando $name")
        def metaMethod = metaClass.getMetaMethod(name, args)
        def result = metaMethod.invoke(this, args)
        System.out.println ("Se terminó de ejecutar $name")
        return 'JUM no te doy nada'
    }

    def dameLasGracias(){
        'gracias :3'
    }
}
