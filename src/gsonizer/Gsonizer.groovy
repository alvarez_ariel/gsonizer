package gsonizer

class Gsonizer {

    def static asegurateDeExistir() {}

    static {
        String.metaClass.toJsonp = {
            "\"${delegate}\""
        }

        Number.metaClass.toJsonp = {
            delegate.toString()
        }

        Boolean.metaClass.toJsonp = {
            delegate.toString()
        }

        Object.metaClass.toJson = {
            delegate.toJsonp()
        }

        Object.metaClass.toJsonp = {
            "{" +
            delegate.properties.
              findAll { name, value ->
                  name != "class" && value != null}.
              sort().
              collect { name, value ->
                  "\"${name}\":${value.toJsonp()}" }.
              join(",") +
            "}"
        }


        Collection.metaClass.toJsonp = {
            "[" +
               delegate.
               collect { object -> object.toJsonp()}.
               join(",") +
            "]"

        }


    }

}
