package examples

import org.junit.Test

/**
 * TADP - UTN FRBA 
 *
 * User: aalvarez
 * Date: 18/05/13
 * Time: 07:51
 */
class InterceptorTest {

    @Test
    void 'El interceptor cambia el valor de retorno de un metodo'(){
        def interceptado = new ClaseConMetodosInterceptados()
        assert interceptado.dameLasGracias() == 'JUM no te doy nada'
    }

}
