package examples

import org.junit.Before
import org.junit.Test

/**
 * TADP - UTN FRBA 
 *
 * User: aalvarez
 * Date: 18/05/13
 * Time: 08:10
 */
class TestDelegador {

    def rojoDelegable


    @Before
    void 'prepara clase Roja mixeada'(){
        rojoDelegable = new Roja();

        rojoDelegable.metaClass.mixin(Delegador)

    }

    @Test
    void 'Roja retorna rojo si no se cambio su responsable'(){
        assert rojoDelegable.color == 'rojo'
    }

    @Test
    void 'Roja retorna azul si su delegable se cambio a instancia de Azul'(){
        rojoDelegable.responsable = new Azul()
        assert rojoDelegable.color == 'azul'
    }

}

class Roja{
    def color = 'rojo'
}

class Azul{
    def color = 'azul'
}