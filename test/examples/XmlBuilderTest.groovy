package examples

import org.junit.Test

/**
 * TADP - UTN FRBA 
 *
 * User: aalvarez
 * Date: 18/05/13
 * Time: 07:37
 */
class XmlBuilderTest {

    @Test
    void "genera html basico con header y body"() {
        def xml = new XmlBuilder(new StringBuffer())
        xml.html {
            head {
                title "Un Título"
            }
            body {
                p "Un lindo mensaje!"
            }
        }

        assert xml.out.toString() == '<html><head><title>Un Título</title></head><body><p>Un lindo mensaje!</p></body></html>'
    }

}
